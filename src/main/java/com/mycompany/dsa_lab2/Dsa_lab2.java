/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.dsa_lab2;

/**
 *
 * @author User
 */
public class Dsa_lab2 {

    static void deleteElementByIndex(int[] arr, int index){
        for(int i=index; i<arr.length-1; i++){
            arr[i]=arr[i+1];
        }
    }

    static void  deleteElementByValue(int[] arr, int value){
        for(int i=0; i<arr.length; i++){
            if(arr[i]==value){
                int temp = i;
                for(int j=temp; j<arr.length-1; j++){
                    arr[j] = arr[j+1];
                }
                break;
            }
        }
    }

    static boolean checkIndex(int[] arr,int index){
        if(index<arr.length){
            return true;
        }else{
            return false;
        }
    }

    static boolean checkValue(int[] arr,int value){
        for(int i=0; i<arr.length; i++){
            if(arr[i]==value){
                return true;
            }
        }
        return false;
    }

    static void reArranged(int[] arr,int[] newArray){
        for(int i=0; i<newArray.length; i++){
            newArray[i] = arr[i];
        }
    }

    static void printArray(int[] arr){
        System.out.print("[");
        for(int i=0; i<arr.length; i++){
            if(i==arr.length-1){
                System.out.print(arr[i]);
            }else{
                 System.out.print(arr[i]+",");
            }
        }
        System.out.println("]");
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int index = 2;
        int value = 4;

        System.out.print("Original Array = ");
        printArray(arr);

        int[] newArr1;
        if(checkIndex(arr,index)){
            newArr1 = new int[arr.length-1];       
        }else{
            newArr1 = new int[arr.length];
        }
        deleteElementByIndex(arr, index);
        reArranged(arr,newArr1);

        System.out.print("Array after deleting element at index "+index+" = ");
        printArray(newArr1);

        int[] newArr2;
        if(checkValue(newArr1,value)){
            newArr2 = new int[newArr1.length-1];
         }else{
             newArr2 = new int[newArr1.length];
         }
        deleteElementByValue(newArr1,value);
        reArranged(newArr1,newArr2);

        
        System.out.print("Array after deleting element with value "+value+" = ");
        printArray(newArr2);
    }
}
